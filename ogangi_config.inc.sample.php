<?php
/**
  * Copyright (c) 2013 Stone Media Group, C.A.
  * @author Alejandro Ricoveri <alejandro@stonemedia.com.ve>
  *
  * Configuración de la API de Ogangi
  */
  
static $ogangi_config = array (
    'debug'       => false,
);

/**
 * Devolver valor de configuración
 */
function ogangi_config ( $value ) {
  global $ogangi_config;
  return isset ($ogangi_config[$value]) ? $ogangi_config[$value] : null;
}
?>
