<?php
/**
  * Copyright (c) 2013 Stone Media Group, C.A.
  * @author Alejandro Ricoveri <alejandro@stonemedia.com.ve>
  * @package ogangi 
  *
  * Librería de utilidades para servicios Ogangi (www.ogangi.com)
  */

require 'ogangi_config.inc.php';

// Variables globales
static $ogangi = array();

/**
 * Obtener valor de una variable global
 * 
 * @global array $ogangi
 * @param string $key Nombre de la variable global
 * @return mixed Valor global bajo la llave $key
 */
function ogangi_get ($key) {
  global $ogangi;
  return $ogangi[$key];
}

/**
 * Establecer valor de una variable global
 * 
 * @global array $ogangi
 * @param string $key Nombre de la variable global
 * @param mixed $value Valor a asignar a la variable global
 */
function ogangi_set ($key, $value) {
  global $ogangi;
  $ogangi[$key] = $value;
}

/**
 * Iniciar documento XML de salida
 */
function ogangi_xml_init() {
  if ( !ogangi_debug() ) 
    // Establecer cabecera XML
    header ("Content-Type: application/xml");
}

/**
 * Determinar si se está en modo de depuración o no
 * 
 * @return boolean 
 */
function ogangi_debug ()
{
  return ogangi_config('debug');
}

/**
 * Iniciar API
 */
function ogangi_init () {
  ogangi_xml_init(); // Iniciar XML
  ogangi_set ( 'sms_text', trim($_GET['text']) );
  ogangi_set ( 'sms_telf', trim($_GET['mobile']) );
}

/**
 * Obtener SMS de entrada
 * 
 * @return string Texto del SMS de entrada
 */
function ogangi_sms_get_text () {
  return ogangi_get ('sms_text');
}

/**
 * Obtener número telefónico de origen del SMS de entrada
 * 
 * @return string Número telefónico del remitente del SMS de entrada
 */
function ogangi_sms_get_telf () {
  return ogangi_get ('sms_telf');
}

/**
 * Establecer mensaje de respuesta en documento XML
 * 
 * @param string $msg Mensaje de respuesta
 */
function ogangi_xml_set_reply ( $msg )
{
  ogangi_set ('xml_reply', ogangi_xml_entities($msg) );
}


// Substitución de caracteres especiales a XML entities equivalentes
function ogangi_xml_entities ($s)
{
  static $patterns      = null;
  static $replacements  = null;
  static $translation   = null;

  if ($translation === null) {
      $translation = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
      foreach ($translation as $k => $v) {
          $patterns[] = "/$v/";
          $replacements[] = '&#' . ord($k) . ';';
      }
  }
  return preg_replace($patterns, $replacements, htmlentities($s, ENT_QUOTES, 'UTF-8'));
}

/** 
 * Escribir documento XML
 */
function ogangi_xml_write () {
  echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
     . "<message><text>" . ogangi_get('xml_reply') . "</text></message>";
}

/** 
 * Mensaje de error
 * 
 * @param string $debug_msg
 */
function ogangi_xml_set_error_reply ( $debug_msg ) {
  ogangi_xml_set_reply( ogangi_debug() ? $debug_msg : "Lo sentimos mucho. Hemos tenido un error interno." );
}
?>
